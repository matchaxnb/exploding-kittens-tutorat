/**
 * Exploding Kittens engine, types and headers
*/
#ifndef EXPLODING_KITTENS_ENGINE
#define EXPLODING_KITTENS_ENGINE

// enum: list of card types

// syntax: https://stackoverflow.com/questions/34132439/typedef-enum-explanation-in-c
typedef enum {
    TYPE_ATTAQUE,
    TYPE_PASSE_TOUR,
    TYPE_FAVEUR,
    TYPE_MELANGE,
    TYPE_DIVINATION,
    TYPE_CHAT,
    TYPE_EXPLODING_KITTEN,
    TYPE_DESAMORCAGE,
    TYPE_NON,
} card_types;

typedef enum {
    PLAYER_STATUS_ALIVE,
    PLAYER_STATUS_DEAD,
}  player_status;

typedef enum{
    GAME_STATUS_IN_PROGRESS,
    GAME_STATUS_OVER,
}  game_status;


// type: card

typedef struct {
    char name [20];
    card_types type;
} CardPrototype;

typedef struct {
    CardPrototype* prototype;
} Card;

// type: pioche (stack)

typedef struct {
    Card cards[56];
} Pioche;

// type: hand (main du joueur)

typedef struct {
    Card cards[8];
} PlayerHand;

// type: player

typedef struct {
    char name[20];
    player_status status;
    PlayerHand *hand;
} Player;


// type: Game

typedef struct {
    unsigned short player_count;
    Player players[5];
    Pioche pioche;
    int player_turn; // c'est le tour de quel joueur?
    game_status status;
} Game;



void create_game(Game *my_game, int nb_players, char player_names[5][20]);

#endif

